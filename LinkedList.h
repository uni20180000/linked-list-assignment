#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED
// CSE221 Assignment 1

#ifndef LinkedList_h
#define LinkedList_h

#include <typeinfo>
#include <iostream>

using namespace std;

template <typename Type>
class LinkedList
{
    struct linked_list
    {
        int number;
        struct linked_list *next;
    };
private:
linked_list* head;
public:
	LinkedList();
	~LinkedList();
	Type Get(const int index);
	void AddAtHead(const Type& val);
	void AddAtIndex(const int index, const Type& val);
	void DeleteAtIndex(const int index);
	void DeleteValue(const Type& val);
	void MoveToHead(const Type& val);
	void Rotate(const int steps);
	void Reduce();
	void Swap();
	int Size();
	void CleanUp();
	void Print();
	void insert_at_last(const Type& val);
};

template <typename Type>
LinkedList<Type>::LinkedList()
{
head=NULL;
}

template <typename Type>
LinkedList<Type>::~LinkedList()
{
if (head!=NULL)
{
    linked_list *temp =head;
    while(temp!=NULL)
    {
        linked_list *next =temp->next;
        delete temp;
        temp=next;
    }
}
}

template <typename Type>
Type LinkedList<Type>::Get(const int index)
{
cout<<endl;
struct linked_list *temp=head;
int len=0,i;
while(temp!=NULL)
{
    if(index<0||index>10){
        return -1;
    }
    else
    {
        if(len==index)
        return(temp->number);
    len++;
    temp=temp->next;
}
}
}
template <typename Type>
void LinkedList<Type>::AddAtHead(const Type& val)
{
     struct linked_list* temp =(struct linked_list*)malloc(sizeof(struct linked_list));
     temp->number = val;
     temp->next =head;
     head = temp;
}
template <typename Type>
void LinkedList<Type>::AddAtIndex(const int index, const Type& val)
{
int i;
struct linked_list* temp1 = (struct linked_list*)malloc(sizeof(struct linked_list));
cout<<endl;
if(index==0)
{
    struct linked_list* temp=(struct linked_list*)malloc(sizeof(struct linked_list));
    temp->number=val;
    temp->next=head;
    head=temp;
}
else
        if(index==10)
        {
        temp1->number=val;
        temp1->next=NULL;
}else if(index<0||index>10)
{
    return;
}
else
{
temp1->number=val;
temp1->next=NULL;
struct linked_list* temp2 = head;
for(i=1;i<=index-1;i++)
    {
temp2=temp2->next;
}
temp1->next=temp2->next;
temp2->next=temp1;
}
}
template <typename Type>
void LinkedList<Type>::DeleteAtIndex(const int index)
{
struct linked_list* temp1=head;
int i;
cout<<endl;
if(index<0||index>10)
    cout<<"the index is invalid.No items will be deleted"<<endl;
else
{
for(i=0;i<index-1;i++)
temp1=temp1->next;
struct linked_list* temp2 = temp1->next;
temp1->next=temp2->next;
free(temp2);
delete(temp2);
}
}
template <typename Type>
void LinkedList<Type>::DeleteValue(const Type& val)
{
linked_list *mynode = head,*prev = NULL;
int flag =0;
while(mynode!=NULL)
{
    if(mynode->number==val)
    {
        if(prev == NULL)
            head= mynode->next;
        else
            prev->next = mynode->next;
        flag = 1;
        free(mynode);
        break;
    }
    prev = mynode;
    mynode = mynode->next;
}
if(flag==0)
    cout<<"key not found";
    cout<<endl;
}

template <typename Type>
void LinkedList<Type>::MoveToHead(const Type& val)
{
struct linked_list * temp_node=(linked_list*)malloc(sizeof(struct linked_list));
struct linked_list* current=head;
struct linked_list* prev;
while(current->number!=val)
{
    prev=current;
    current=current->next;
}
prev->next=current->next;
temp_node->number=val;
temp_node->next=head;
head=temp_node;
cout<<endl;
}
template <typename Type>
void LinkedList<Type>::Rotate(const int steps)
{
if(steps<0)
    return;
struct linked_list* temp;
temp=head;
int count=1;
while(count <9-steps && temp!=NULL)
{
    temp=temp->next;
    count++;
}
if(temp==NULL)
    return;
struct linked_list *kthnode = temp;
while(temp->next!=NULL)
    temp=temp->next;
temp->next=head;
head=kthnode->next;
kthnode->next=NULL;
cout<<endl;
}
template <typename Type>
void LinkedList<Type>::Reduce()
{
struct linked_list* temp1=head;
struct linked_list *temp2,*dup;
while(temp1!=NULL && temp1->next!=NULL)
{
    temp2=temp1;
while(temp2->next!=NULL)
{
    if(temp1->number==temp2->next->number)
    {
        dup=temp2->next;
        temp2->next=temp2->next->next;
        delete(dup);
    }
    else
        temp2=temp2->next;
}
temp1=temp1->next;
}
cout<<endl;
}

template <typename Type>
void LinkedList<Type>::Swap()
{
struct linked_list *temp=head;
while(temp!=NULL && temp->next!=NULL)
{
    swap(temp->number,temp->next->number);
    temp=temp->next->next;
}
cout<<endl;
}

template <typename Type>
int LinkedList<Type>::Size()
{
struct linked_list *temp = head;
int c=0;
cout<<endl;
while(temp!=NULL)
{
    c++;
    temp=temp->next;
}
return c;
}
template <typename Type>
void LinkedList<Type>::CleanUp()
{
    struct linked_list *temp =head;
    head=head->next;
    while((head) !=NULL)
    {
        free(temp);
        temp=head;
        head=head->next;

    }
}
template <typename Type>
void LinkedList<Type>::Print()
{
    linked_list *myList;
    myList =head;
std::cout<<"(";
while(myList!=NULL)
    {
std::cout<<myList->number<<",";
    myList =myList->next;
}
std::cout<<")";
}


#endif



#endif // LINKEDLIST_H_INCLUDED
